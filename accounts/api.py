from django.contrib.auth import authenticate, login, logout
from rest_framework import decorators, status, viewsets
from rest_framework.response import Response
from rules.contrib.rest_framework import AutoPermissionViewSetMixin

from . import serializers
from .models import User


class CustomUserViewset(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = User.objects.none()
    serializer_class = serializers.CustomUserSerializer

    permission_type_map = {
        "list": "list",
        "create": "add",
        "destroy": "delete",
        "partial_update": "change",
        "retrieve": "view",
        "update": "change",
        "register": "register",
        "login": "login",
        "logout": "logout",
    }

    def get_queryset(self):
        current_user = self.request.user
        if current_user.is_superuser:
            # Superuser can see everyone
            return User.objects.all()
        # If you are not a superuser, You can only see yourself
        return User.objects.filter(pk=current_user.pk)

    def get_object(self):
        if self.kwargs["pk"] == "me":
            # Make it so that calling /accounts/api/users/me return info about current user
            self.kwargs["pk"] = self.request.user.pk
        if self.kwargs["pk"] is None:
            return User(is_active=False)
        return super().get_object()

    @decorators.action(methods=["post"], detail=False)
    def register(self, request):
        register_serializer = serializers.RegisterSerializer(data=request.data)

        register_serializer.is_valid(raise_exception=True)

        username = register_serializer.validated_data["username"]
        password = register_serializer.validated_data["password"]
        # email = register_serializer.validated_data["email"] // for now, email is auto-generated
        email = register_serializer.validated_data.get("email") or f"{username}@example.com"

        User.objects.create_user(username=username, email=email, password=password)
        return Response(status=status.HTTP_201_CREATED)

    @decorators.action(methods=["post"], detail=False)
    def login(self, request):
        login_serializer = serializers.LoginSerializer(data=request.data)

        login_serializer.is_valid(raise_exception=True)

        username = login_serializer.validated_data["username"]
        password = login_serializer.validated_data["password"]
        user = authenticate(request, username=username, password=password)

        if user:
            login(request, user)
            custom_user_serializer = self.get_serializer(user)
            return Response(
                custom_user_serializer.data,
                status=status.HTTP_200_OK,
            )

        return Response({"detail": "Invalid username or password"}, status=status.HTTP_401_UNAUTHORIZED)

    @decorators.action(detail=False, methods=["get"])
    def logout(self, request):
        logout(request)
        return Response({"detail": "Successfully logged out"})
