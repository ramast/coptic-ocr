import typing

import rules

if typing.TYPE_CHECKING:
    from .models import User


@rules.predicate(bind=True)
def is_admin(self, user: "User"):
    return rules.is_active(user) if user.is_superuser else False


@rules.predicate(bind=True)
def is_same_user(self, auth_user: "User", target_user: "User"):
    """
    Is authenticated user the same user as target_user?
    """
    return auth_user.is_active and auth_user.pk == target_user.pk


rules.add_perm("accounts.view_user", is_admin | is_same_user)
rules.add_perm("accounts.list_user", rules.is_active)
rules.add_perm("accounts.change_user", is_admin | is_same_user)
rules.add_perm("accounts.add_user", is_admin)
rules.add_perm("accounts.delete_user", is_admin)
rules.add_perm("accounts.login_user", rules.predicates.always_allow)
rules.add_perm("accounts.register_user", rules.predicates.always_allow)
rules.add_perm("accounts.logout_user", rules.is_active)
