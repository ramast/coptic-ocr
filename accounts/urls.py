from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import api

router = DefaultRouter()
router.register("users", api.CustomUserViewset)

urlpatterns = [
    path("api/", include(router.urls)),
]
