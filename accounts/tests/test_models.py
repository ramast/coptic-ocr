import typing

import pytest
from rest_framework.test import APIClient

from ocr import constants
from ocr.models import OCRPageTranscription
from ocr.tests import factories as ocr_factories

from . import factories

if typing.TYPE_CHECKING:
    from accounts.models import User


@pytest.mark.django_db(transaction=True)
@pytest.mark.parametrize("is_superuser", [True, False])
def test_user_str(is_superuser):
    user: "User" = factories.UserFactory(is_superuser=is_superuser, password="test")
    if is_superuser:
        assert str(user) == "[ADMIN] user_1"
    else:
        assert str(user) == "user_1"


def test_user_full_name_or_username():
    user_with_name: "User" = factories.UserFactory.build(username="abc", first_name="Ken", last_name="Adams")
    assert user_with_name.full_name_or_username == "Ken Adams"

    user_without_name: "User" = factories.UserFactory.build(username="abc")
    assert user_without_name.full_name_or_username == "abc"


@pytest.mark.django_db(transaction=True)
def test_user_score():
    user1 = factories.UserFactory(username="user1")
    user2 = factories.UserFactory(username="user2")
    admin = factories.AdminUserFactory(username="admin")

    user1_client = APIClient()
    user2_client = APIClient()
    admin_client = APIClient()

    user1_client.force_authenticate(user1)
    user2_client.force_authenticate(user2)
    admin_client.force_authenticate(admin)

    # There should be a page, with two different transcriptions from two users
    page1 = ocr_factories.OCRPageFactory(
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"
    )
    page2 = ocr_factories.OCRPageFactory(
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"
    )
    page3 = ocr_factories.OCRPageFactory(
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"
    )

    ocr_factories.OCRPageTranscriptionFactory(
        author=user1,
        page=page1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3 with extra text\ntranscription_line4",
        status=constants.TranscriptionStatus.submitted,
        approved_lines_count=3,
        rejected_lines_count=1,
    )

    ocr_factories.OCRPageTranscriptionFactory(
        author=user1,
        page=page2,
        text="transcription_line1\ntranscription_line2\ntranscription_line3 with extra text\ntranscription_line4",
        status=constants.TranscriptionStatus.pending,
        approved_lines_count=0,
        rejected_lines_count=0,
    )

    ocr_factories.OCRPageTranscriptionFactory(
        author=user1,
        page=page3,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4",
        status=constants.TranscriptionStatus.submitted,
        approved_lines_count=4,
        rejected_lines_count=0,
    )

    ocr_factories.OCRPageTranscriptionFactory(
        author=user2,
        page=page1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4",
        status=constants.TranscriptionStatus.submitted,
        approved_lines_count=4,
        rejected_lines_count=0,
    )

    # Now each user should have a score, that is equal to:
    # approved_lines_count - 10 * rejected_lines_count
    # So, user2 should have a score of 4 (total number of lines user2 got correct)
    # And user1, should have a score of 3 - 10 * 1 + 4 = -3

    assert user1.user_score == -3
    assert user2.user_score == 4

    # now if we delete the transcriptions of user2, user_score should be zero for user2
    # while, user_score for user1 is still -3

    OCRPageTranscription.objects.filter(author=user2, page=page1).delete()

    assert user1.user_score == -3
    assert user2.user_score == 0
