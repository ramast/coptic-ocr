import pytest
from rest_framework.test import APIClient

from . import factories

api_client = APIClient()


@pytest.mark.django_db
@pytest.mark.parametrize("is_superuser", [True, False])
def test_listing_users(is_superuser):
    user = factories.UserFactory(is_superuser=is_superuser, username="current_user")

    # Create another user
    factories.UserFactory()
    # Test listing users
    api_client.force_authenticate(user)
    response = api_client.get("/b/accounts/api/users/")
    assert response.status_code == 200, response
    if is_superuser:
        assert response.json() == {
            "count": 2,
            "next": None,
            "previous": None,
            "results": [
                {
                    "about_me": "",
                    "email": "current_user@example.com",
                    "first_name": "",
                    "last_name": "",
                    "username": "current_user",
                    "is_active": True,
                    "is_staff": True,
                    "user_contributions": [],
                    "user_score": 0,
                },
                {
                    "about_me": "",
                    "email": "user_1@example.com",
                    "first_name": "",
                    "last_name": "",
                    "username": "user_1",
                    "is_active": True,
                    "is_staff": False,
                    "user_contributions": [],
                    "user_score": 0,
                },
            ],
        }
    else:  # Normal user
        assert response.json() == {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "about_me": "",
                    "email": "current_user@example.com",
                    "first_name": "",
                    "last_name": "",
                    "username": "current_user",
                    "is_active": True,
                    "is_staff": False,
                    "user_contributions": [],
                    "user_score": 0,
                },
            ],
        }


@pytest.mark.django_db
def test_get_current_user_details():
    # first try without logging in
    api_client.logout()  # first making sure that api_client is not logged in
    response = api_client.get("/b/accounts/api/users/me/?fields=is_active,is_staff")
    assert response.status_code == 403, response
    assert response.json() == {"detail": "Please login to view this section"}

    # Then creating a new user to login
    user = factories.UserFactory(username="current_user", first_name="a", last_name="b")
    # Create another user
    factories.UserFactory()
    # Test listing users
    api_client.force_authenticate(user)
    response = api_client.get("/b/accounts/api/users/me/")
    assert response.status_code == 200, response
    assert response.json() == {
        "about_me": "",
        "email": "current_user@example.com",
        "first_name": "a",
        "last_name": "b",
        "username": "current_user",
        "is_active": True,
        "is_staff": False,
        "user_contributions": [],
        "user_score": 0,
    }


@pytest.mark.django_db
def test_create_user():
    # Test creating user
    user = factories.AdminUserFactory(username="admin_user", first_name="a", last_name="b")
    api_client.force_authenticate(user)
    response = api_client.post(
        "/b/accounts/api/users/",
        {"username": "new_user", "email": "test@example.com", "is_staff": True, "is_active": False},
    )
    assert response.status_code == 201, response
    # is staff, is active should be ignored
    assert response.json() == {
        "about_me": "",
        "email": "test@example.com",
        "first_name": "",
        "last_name": "",
        "username": "new_user",
        "is_active": True,
        "is_staff": False,
        "user_contributions": [],
        "user_score": 0,
    }
    # Test creating user as normal user
    user = factories.UserFactory(username="current_user", first_name="a", last_name="b")
    api_client.force_authenticate(user)
    response = api_client.post("/b/accounts/api/users/", {"username": "new_user"})
    assert response.status_code == 403, response


@pytest.mark.django_db
def test_update_user_profile():
    # normal user should be able to update his profile
    user = factories.UserFactory(username="normal_user", first_name="a", last_name="b")
    api_client.force_authenticate(user)

    # first check for default user info
    response = api_client.get("/b/accounts/api/users/me/")
    assert response.status_code == 200, response
    assert response.json() == {
        "about_me": "",
        "email": "normal_user@example.com",
        "first_name": "a",
        "last_name": "b",
        "username": "normal_user",
        "is_active": True,
        "is_staff": False,
        "user_contributions": [],
        "user_score": 0,
    }

    # Now, update user profile
    response = api_client.put(
        "/b/accounts/api/users/me/",
        {
            "username": "normal_user",
            "email": "new_email@example.com",
            "first_name": "c",
            "last_name": "d",
            "about_me": "Some text here",
            "is_staff": True,
            "is_active": False,
        },
    )
    assert response.status_code == 200, response
    # check for the new user info in the response
    # is_staff, is_active should be ignored
    assert response.json() == {
        "about_me": "Some text here",
        "email": "new_email@example.com",
        "first_name": "c",
        "last_name": "d",
        "username": "normal_user",
        "is_active": True,
        "is_staff": False,
        "user_contributions": [],
        "user_score": 0,
    }


@pytest.mark.django_db
def test_register_user():
    challenge = "ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ."
    body_correct = {"username": "user1", "password": "password1", "challenge": challenge, "email": "test@yahoo.com"}
    body_with_invalid_username = {"username": "user-1", "password": "password2", "challenge": challenge}
    body_with_existing_username = {
        "username": "user1",
        "password": "password3",
        "challenge": challenge,
    }
    body_with_existing_email = {
        "username": "user7",
        "password": "password3",
        "challenge": challenge,
        "email": "test@yahoo.com",
    }

    # Registering body_correct should be ok:
    response = api_client.post("/b/accounts/api/users/register/", body_correct)
    assert response.status_code == 201, response.json()

    #  registering body_with_invalid_username should give validation error:
    response = api_client.post("/b/accounts/api/users/register/", body_with_invalid_username)
    assert response.status_code == 400, response.json()
    assert response.json() == {
        "username": ["Enter a valid username. Username may contain only lower case letters, numbers, and _ characters."]
    }

    #  registering body_with_existing_username should give user_name not unique error:
    #  first create the first user:
    api_client.post("/b/accounts/api/users/register/", body_correct)

    # then try to create another user with the same username:
    response = api_client.post("/b/accounts/api/users/register/", body_with_existing_username)
    assert response.status_code == 400, response
    assert response.json() == {"username": ["This username is already in use."]}
    # then try to create another user with the same email:
    response = api_client.post("/b/accounts/api/users/register/", body_with_existing_email)
    assert response.status_code == 400, response
    assert response.json() == {"email": ["This email is already in use."]}


@pytest.mark.django_db
def test_login_user():
    # first create the user
    factories.UserFactory(username="first_user", password="password")
    # then try to login
    response = api_client.post("/b/accounts/api/users/login/", {"username": "first_user", "password": "password"})
    assert response.status_code == 200, response
    assert response.json() == {
        "username": "first_user",
        "first_name": "",
        "last_name": "",
        "email": "first_user@example.com",
        "is_active": True,
        "is_staff": False,
        "user_contributions": [],
        "user_score": 0,
        "about_me": "",
    }

    # login with incorrect username
    factories.UserFactory(username="second_user", password="password")
    response = api_client.post("/b/accounts/api/users/login/", {"username": "another_username", "password": "password"})
    assert response.status_code == 401, response
    assert response.json() == {"detail": "Invalid username or password"}

    # login with incorrect password
    response = api_client.post("/b/accounts/api/users/login/", {"username": "second_user", "password": "password1"})
    assert response.status_code == 401, response
    assert response.json() == {"detail": "Invalid username or password"}


@pytest.mark.django_db
def test_logout_user():
    # first create the user and login
    first_user = factories.UserFactory(username="first_user", password="password")
    api_client.force_authenticate(first_user)

    # now try to logout
    response = api_client.get("/b/accounts/api/users/logout/")
    assert response.status_code == 200, response
    assert response.json() == {"detail": "Successfully logged out"}
