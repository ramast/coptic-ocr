from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers

from ocr import constants as ocr_constants
from ocr import models as ocr_models
from ocr.serializers import OCRPageSerializer

from .models import User
from .validators import PasswordValidator, UsernameValidator


class CustomUserSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    is_staff = serializers.SerializerMethodField()
    user_score = serializers.SerializerMethodField()
    user_contributions = serializers.SerializerMethodField()

    @staticmethod
    def get_is_staff(user: "User"):
        return user.is_staff or user.is_superuser

    class Meta:
        model = User
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "is_active",
            "is_staff",
            "about_me",
            "user_score",
            "user_contributions",
        )
        read_only_fields = ("is_active", "is_staff")

    @staticmethod
    def get_user_score(user: "User"):
        return user.user_score

    @staticmethod
    def get_user_contributions(user: "User"):
        contributed_pages = ocr_models.OCRPage.objects.filter(
            transcriptions__author=user,
            transcriptions__status__in=(
                ocr_constants.TranscriptionStatus.submitted,
                ocr_constants.TranscriptionStatus.auto_approved,
                ocr_constants.TranscriptionStatus.manually_approved,
            ),
        ).order_by("transcriptions__created", "pk")
        return OCRPageSerializer(contributed_pages, many=True).data

    def update(self, instance, validated_data):
        # Username can't be altered
        validated_data.pop("username")
        return super().update(instance, validated_data)


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=30, validators=[UsernameValidator()])
    email = serializers.EmailField(required=False, allow_blank=True)
    password = serializers.CharField(max_length=128, validators=[PasswordValidator()])
    challenge = serializers.CharField(max_length=255)

    # check for the unique username
    @staticmethod
    def validate_username(username):
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError("This username is already in use.")
        return username

    # check for the unique email
    @staticmethod
    def validate_email(email):
        """
        >>> import pytest
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_email("test@example.com")
        """
        if "@example.com" in email:
            raise serializers.ValidationError("Please provide a valid email address.")
        if email and User.objects.filter(email=email).exists():
            raise serializers.ValidationError("This email is already in use.")
        return email

    @staticmethod
    def validate_challenge(challenge: str):  # noqa: WPS231, WPS238
        """
        >>> import pytest
        >>> with pytest.raises(serializers.ValidationError):
        ...    RegisterSerializer.validate_challenge("too short")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge(" ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. `ⲛϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. ̀ⲛϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ...")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦  ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϦⲈⲚ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲧⲉⲙⲙⲁⲩ.")
        >>> with pytest.raises(serializers.ValidationError):
        ...     RegisterSerializer.validate_challenge("ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ fofofo.")
        """
        if challenge.strip() == "ⲇ︦. ⲁ. ⲛ̀ϩⲣⲏⲓ ⲇⲉ ϧⲉⲛ ⲡⲓⲥⲏⲟⲩ ⲉ̀ⲧⲉⲙⲙⲁⲩ.":
            return ""
        if len(challenge) < 10:
            raise serializers.ValidationError("Please type in the full text")

        # in this section, I try to guess what the user did wrong and try to suggest correction.
        # 1. no capitals
        for character in challenge:
            if character.isupper():
                raise serializers.ValidationError("Capital letters are not allowed in this webiste")
        # 2. ensure user knows how to write dash
        if challenge[0] != "ⲇ" or challenge[1] != "︦":
            raise serializers.ValidationError(
                "First letter is delta with a dash on top. if you are using the online editor, type 'd=' there."
            )
        if "`" in challenge:
            raise serializers.ValidationError(
                "You are using wrong character for the jenkim. please try using the online editor"
            )
        if " ̀" in challenge:
            raise serializers.ValidationError(
                "Jenkim should be written after the letter not before it." "i.e ⲛ̀ϩⲣⲏⲓ not ̀ⲛϩⲣⲏⲓ"
            )
        if challenge.count(".") != 3:
            raise serializers.ValidationError("There is exactly 3 periods in the text")
        if challenge.count("̀") != 2:
            raise serializers.ValidationError("There is exactly 2 jenkims in the text")

        raise serializers.ValidationError("The text you entered was incorrect")


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=30)
    password = serializers.CharField(max_length=128)
