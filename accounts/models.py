from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as BuiltInUserManager
from django.db import models
from django.utils.translation import gettext as _
from rules.contrib import models as rules_models

from accounts.validators import UsernameValidator


class UserManager(BuiltInUserManager):
    def create(self, **kwargs):
        password = kwargs.pop("password", None)
        user = User(**kwargs)
        if password:
            user.set_password(password)
        user.save()
        return user


class User(rules_models.RulesModelMixin, AbstractUser, metaclass=rules_models.RulesModelBase):
    username = models.CharField(
        "username",
        max_length=30,
        unique=True,
        help_text=_("Required. 30 characters or less. Letters, digits and _ only."),
        validators=[UsernameValidator()],
        error_messages={"unique": "A user with that username already exists."},
    )
    email = models.EmailField(_("email address"), blank=True, unique=True)
    about_me = models.TextField(blank=True, default="")

    objects = UserManager()  # noqa: WPS110

    class Meta:
        ordering = ("pk",)

    def __str__(self):
        if self.is_superuser:
            return f"[ADMIN] {self.username}"
        return self.username

    @property
    def full_name_or_username(self):
        return self.get_full_name() or self.username

    # property to calculate user's total score =>
    # approved lines minus rejected lines multiplied by 10
    @property
    def user_score(self):
        scores = self.user_transcriptions.aggregate(
            approved=models.Sum("approved_lines_count"),
            rejected=models.Sum("rejected_lines_count"),
        )
        approved_count = scores["approved"] or 0
        rejected_count = scores["rejected"] or 0
        return approved_count - rejected_count * 10
