from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible


@deconstructible
class UsernameValidator(RegexValidator):
    regex = "^[a-z0-9_]{4,}$"
    message = "Enter a valid username. Username may contain only lower case letters, numbers, and _ characters."
    flags = 0


@deconstructible
class PasswordValidator(RegexValidator):
    regex = r"^(?=.*[a-zA-Z])(?=.*\d).{8,}$"
    message = "Password must be at least 8 characters long and contain at least one number."
    flags = 0
