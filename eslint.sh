set -e
files="$(echo "$@"|sed 's:frontend/::g')"
cd frontend

#npm run format --write $files > /dev/null
npm run lint --fix $files
