import pytest
from django.core import exceptions

from accounts.tests import factories as accounts_factories
from ocr import constants, models

from . import factories


@pytest.mark.django_db(transaction=True)
def test_ocr_document_str():
    document: "models.OCRDocument" = factories.OCRDocumentFactory()
    assert str(document) == "Book 0"


@pytest.mark.django_db(transaction=True)
def test_ocr_page_str():
    page: "models.OCRPage" = factories.OCRPageFactory(number=7)
    assert str(page) == "Book 0 (7)"


@pytest.mark.django_db(transaction=True)
def test_ocr_page_best_transcription():
    user1 = accounts_factories.UserFactory()
    user2 = accounts_factories.UserFactory()

    page1: "models.OCRPage" = factories.OCRPageFactory()
    page2: "models.OCRPage" = factories.OCRPageFactory()

    # Auto approved pages take precedence over submitted pages
    page1.set_text("Submitted1\nSubmitted2", user1, constants.TranscriptionStatus.submitted)
    page1.set_text("line1\nline2", user2, constants.TranscriptionStatus.auto_approved)
    assert page1.best_transcription == "line1\nline2"

    # If auto_approved page is not available, fall back to submitted pages
    page1.transcriptions.filter(author=user2).update(status=constants.TranscriptionStatus.pending)
    assert page1.best_transcription == "Submitted1\nSubmitted2"

    # If no transcription available for certain line, return empty string
    page1.transcriptions.filter(author=user1).update(status=constants.TranscriptionStatus.pending)
    assert page1.best_transcription == ""

    # Pending lines are always ignored
    assert page2.best_transcription == ""
    page2.set_text("lineA\nlineB", user1)
    assert page2.best_transcription == ""


@pytest.mark.django_db(transaction=True)
def test_ocr_page_transcription():
    user1 = accounts_factories.UserFactory()
    user2 = accounts_factories.UserFactory()
    page1: "models.OCRPage" = factories.OCRPageFactory(text="ocr line")
    factories.OCRPageTranscriptionFactory(page=page1, text="user1 transcription", author=user1)
    assert page1.get_text(user1) == "user1 transcription"
    assert page1.get_text() == ""
    assert page1.get_text(user2) == ""


@pytest.mark.django_db(transaction=True)
def test_ocr_transcription_set_text():
    user1 = accounts_factories.UserFactory()
    user2 = accounts_factories.UserFactory()
    page1: "models.OCRPage" = factories.OCRPageFactory()
    page2: "models.OCRPage" = factories.OCRPageFactory()
    # Attempting to assign more or less lines than what the page currently have
    with pytest.raises(exceptions.ValidationError):
        page1.set_text("One line", user1)
    with pytest.raises(exceptions.ValidationError):
        page1.set_text("1 line\n2 Line\n3 Line", user1)

    example_text = "1 line\n2 Line"
    # Correct call
    page1.set_text(example_text, user1)
    # verify it worked
    assert page1.get_text(user1) == example_text
    # Test updating existing text
    example_text = "1 new line\n2 new Line"
    page1.set_text(example_text, user1)
    assert page1.get_text(user1) == example_text
    # Test if changes to page2 could influence page1's text or not
    page2.set_text("Hello\nWorld", user1)
    assert page2.get_text(user1) == "Hello\nWorld"
    assert page1.get_text(user1) == example_text
    # Test auto approval capabilities
    assert page1.best_transcription == ""
    page1.set_text(example_text, user1, constants.TranscriptionStatus.submitted)
    page1.set_text(example_text, user2, constants.TranscriptionStatus.submitted)
    assert page1.best_transcription == example_text


@pytest.mark.django_db(transaction=True)
def test_ocr_transcription_auto_approve():
    user1 = accounts_factories.UserFactory()
    user2 = accounts_factories.UserFactory()
    user3 = accounts_factories.UserFactory()
    page1 = factories.OCRPageFactory()
    page2 = factories.OCRPageFactory()
    # Two transcriptions from different users with agreeing transcription
    transcription1: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page1,
        text="abc\n line2",
        author=user1,
        status=constants.TranscriptionStatus.submitted,
    )
    transcription2: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page1,
        text="abc\n line2",
        author=user2,
        status=constants.TranscriptionStatus.submitted,
    )
    # One transcription from another user with not agreeing transcription
    transcription3: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page1,
        text="cbc\n line2",
        author=user3,
        status=constants.TranscriptionStatus.submitted,
    )
    # Two transcriptions from different users with not agreeing transcription, both should be submitted
    transcription4: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page2,
        text="abc\n line2",
        author=user1,
        status=constants.TranscriptionStatus.submitted,
    )
    transcription5: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page2,
        text="xyz\n line2",
        author=user2,
        status=constants.TranscriptionStatus.submitted,
    )

    page1.auto_approve()
    page2.auto_approve()
    # Pages originally are pending
    assert transcription1.status == constants.TranscriptionStatus.submitted
    assert transcription2.status == constants.TranscriptionStatus.submitted
    assert transcription3.status == constants.TranscriptionStatus.submitted
    assert transcription4.status == constants.TranscriptionStatus.submitted
    assert transcription5.status == constants.TranscriptionStatus.submitted
    # Pages are auto approved after refreshing from db
    transcription1.refresh_from_db()
    transcription2.refresh_from_db()
    transcription3.refresh_from_db()
    transcription4.refresh_from_db()
    transcription5.refresh_from_db()
    # These two transcriptions should be auto_approved, as they were agreeing
    assert transcription1.status == constants.TranscriptionStatus.auto_approved
    assert transcription2.status == constants.TranscriptionStatus.auto_approved
    # transcription3 differ from the other two so it will be rejected
    assert transcription3.status == constants.TranscriptionStatus.rejected

    transcription4.refresh_from_db()
    transcription5.refresh_from_db()
    assert transcription4.status == constants.TranscriptionStatus.submitted
    assert transcription5.status == constants.TranscriptionStatus.submitted


@pytest.mark.django_db(transaction=True)
def test_ocr_transcription_auto_mark_page_complete():
    """
    Test if auto_approve function automatically mark page as complete if two transcriptions agree
    """

    user1 = accounts_factories.UserFactory()
    user2 = accounts_factories.UserFactory()
    user3 = accounts_factories.UserFactory()
    page: "models.OCRPage" = factories.OCRPageFactory()
    # Submitting two different transcription, it should NOT auto approve and auto complete
    transcription1: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page,
        text="abc\n line2",
        author=user1,
        status=constants.TranscriptionStatus.submitted,
    )
    transcription2: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page,
        text="def\n line2",
        author=user2,
        status=constants.TranscriptionStatus.submitted,
    )
    page.refresh_from_db()
    page.auto_approve()

    # Page is NOT complete:
    assert not page.complete

    # Both transcriptions are submitted (because they are not agreeing, so their status is not changed)
    transcription1.refresh_from_db()
    transcription2.refresh_from_db()
    assert transcription1.status == constants.TranscriptionStatus.submitted
    assert transcription2.status == constants.TranscriptionStatus.submitted

    # The third transcription agrees with the first one
    transcription3: models.OCRPageTranscription = factories.OCRPageTranscriptionFactory(
        page=page,
        text="abc\n line2",
        author=user3,
        status=constants.TranscriptionStatus.submitted,
    )
    page.auto_approve()
    page.refresh_from_db()
    transcription1.refresh_from_db()
    transcription2.refresh_from_db()
    transcription3.refresh_from_db()

    # Now the page should be complete
    # and transcription1 and transcription3 should be auto_approved
    # and transcription2 should still be rejected

    assert page.complete
    assert transcription1.status == constants.TranscriptionStatus.auto_approved
    assert transcription2.status == constants.TranscriptionStatus.rejected
    assert transcription3.status == constants.TranscriptionStatus.auto_approved
