import factory
from factory.django import DjangoModelFactory

from accounts.tests.factories import UserFactory
from ocr import constants
from ocr import models as ocr_models


class OCRDocumentFactory(DjangoModelFactory):
    filename = "my_book.pdf"
    image = "image_url.jpg"
    description = "Book description"

    @factory.lazy_attribute
    def title(self):
        book_count = ocr_models.OCRDocument.objects.count()
        return f"Book {book_count}"

    class Meta:
        model = "ocr.OCRDocument"


class OCRPageFactory(DjangoModelFactory):
    document = factory.SubFactory(OCRDocumentFactory)
    number = 1
    text = "Line \n #1"
    lines_data = [{"box": [278, 1098, 355, 1141]}, {"box": [971, 1105, 983, 1133]}]

    @factory.lazy_attribute
    def image(self):
        return f"books/page-{self.number}.jpg"

    class Meta:
        model = "ocr.OCRPage"


class OCRPageTranscriptionFactory(DjangoModelFactory):
    author = factory.SubFactory(UserFactory)
    page = factory.SubFactory(OCRPageFactory)

    class Meta:
        model = "ocr.OCRPageTranscription"


class OCRTranslationFactory(DjangoModelFactory):
    status = constants.TranslationStatus.submitted
    number = 1
    text = "test1"
    comment = ""
    translation = "test1 translation"

    class Meta:
        model = "ocr.OCRLineTranslation"
