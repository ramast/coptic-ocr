import pytest

from accounts.tests.factories import UserFactory


@pytest.mark.django_db(transaction=True)
def test_user_add_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("ocr.add_ocrdocument")
    assert not normal_user.has_perm("ocr.add_ocrdocument")


@pytest.mark.django_db(transaction=True)
def test_user_delete_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("ocr.delete_ocrdocument")
    assert not normal_user.has_perm("ocr.delete_ocrdocument")


@pytest.mark.django_db(transaction=True)
def test_user_read_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("ocr.view_ocrdocument")
    assert normal_user.has_perm("ocr.view_ocrdocument")


@pytest.mark.django_db(transaction=True)
def test_user_change_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("ocr.change_ocrdocument")
    assert not normal_user.has_perm("ocr.change_ocrdocument")
