import pytest
from freezegun import freeze_time
from rest_framework.test import APIClient

from accounts.tests import factories as accounts_factories
from ocr import constants
from ocr.models import OCRPage, OCRPageTranscription

from . import factories


@pytest.mark.django_db
def test_listing_documents():
    api_client = APIClient()
    document = factories.OCRDocumentFactory()
    document2 = factories.OCRDocumentFactory(title="Book2")
    for page_number in range(1, 4):
        factories.OCRPageFactory(number=page_number, document=document)
    first_page = OCRPage.objects.filter(document=document).order_by("pk").first()
    response = api_client.get("/b/ocr/api/documents/")
    assert response.status_code == 200, response
    assert response.json() == [
        {
            "pk": document2.pk,
            "title": "Book2",
            "first_page_id": None,
            "image": "/media/image_url.jpg",
            "filename": "/media/my_book.pdf",
            "description": "Book description",
        },
        {
            "pk": document.pk,
            "title": "Book 0",
            "first_page_id": first_page.pk,
            "image": "/media/image_url.jpg",
            "filename": "/media/my_book.pdf",
            "description": "Book description",
        },
    ]


@pytest.mark.django_db
def test_document_details():
    api_client = APIClient()
    page = factories.OCRPageFactory()
    document_id = page.document_id
    response = api_client.get(f"/b/ocr/api/documents/{document_id}/")
    assert response.status_code == 200, response
    assert response.json() == {
        "pk": document_id,
        "title": "Book 0",
        "url": "/media/my_book.pdf",
        "description": "Book description",
        "pages": [
            {
                "document_id": document_id,
                "document_title": "Book 0",
                "pk": page.pk,
                "number": 1,
                "url": "/media/books/page-1.jpg",
            }
        ],
    }


@pytest.mark.django_db
def test_page_details():
    api_client = APIClient()

    def get_page_details_response():
        return api_client.get(f"/b/ocr/api/pages/{page.pk}/")

    user = accounts_factories.UserFactory()
    user_with_approved_transcription = accounts_factories.UserFactory()
    document = factories.OCRDocumentFactory()
    page = factories.OCRPageFactory(document=document, number=1, text="ocr line")
    factories.OCRPageTranscriptionFactory(
        author=user_with_approved_transcription,
        page=page,
        text="approved line",
        status=constants.TranscriptionStatus.manually_approved,
    )
    response = get_page_details_response()
    assert response.status_code == 200, response
    assert response.json() == {
        "pk": page.pk,
        "number": 1,
        "url": "/media/books/page-1.jpg",
        "ocr_text": "ocr line",
        "total_number_of_transcriptions": 1,
        "transcription": "",
        "submitted": False,
        "complete": False,
        "next_page_complete": True,  # there is no next_page, so it returns true
        "lines_data": [
            {"box": [278, 1098, 355, 1141]},
            {"box": [971, 1105, 983, 1133]},
        ],
    }
    # Test page details with a loggedIn user who has provided transcription text
    api_client.force_authenticate(user)
    page.set_text("My Transcription", user, constants.TranscriptionStatus.submitted)
    response = get_page_details_response()
    assert response.json() == {
        "pk": page.pk,
        "number": 1,
        "url": "/media/books/page-1.jpg",
        "ocr_text": "ocr line",
        "total_number_of_transcriptions": 2,
        "transcription": "My Transcription",
        "submitted": True,
        "complete": False,
        "next_page_complete": True,  # there is no next_page, so it returns true
        "lines_data": [
            {"box": [278, 1098, 355, 1141]},
            {"box": [971, 1105, 983, 1133]},
        ],
    }
    # if the next page exists and it is complete, next_page_complete must be True
    factories.OCRPageFactory(document=document, complete=True, number=2)
    response = api_client.get(f"/b/ocr/api/pages/{page.pk}/")
    assert response.json() == {
        "pk": page.pk,
        "number": 1,
        "url": "/media/books/page-1.jpg",
        "ocr_text": "ocr line",
        "total_number_of_transcriptions": 2,
        "transcription": "My Transcription",
        "submitted": True,
        "complete": False,
        "next_page_complete": True,  # next_page (complete_page) is complete, so it returns true
        "lines_data": [
            {"box": [278, 1098, 355, 1141]},
            {"box": [971, 1105, 983, 1133]},
        ],
    }
    # If current page is complete, transcription key should return the officially accepted transcription
    page.complete = True
    page.save()
    response = api_client.get(f"/b/ocr/api/pages/{page.pk}/")
    assert response.json() == {
        "pk": page.pk,
        "number": 1,
        "url": "/media/books/page-1.jpg",
        "ocr_text": "ocr line",
        "total_number_of_transcriptions": 2,
        "transcription": "approved line",
        "submitted": True,
        "complete": True,
        "next_page_complete": True,  # next_page (complete_page) is complete, so it returns true
        "lines_data": [
            {"box": [278, 1098, 355, 1141]},
            {"box": [971, 1105, 983, 1133]},
        ],
    }


@pytest.mark.django_db
def test_setting_transcriptions():
    page = factories.OCRPageFactory()
    user = accounts_factories.UserFactory(is_active=True)

    client = APIClient()
    client.force_authenticate(user)

    assert page.get_text(user) == ""
    response = client.post(f"/b/ocr/api/pages/{page.pk}/submit_transcription/", {"text": "Test Submission\n new line"})
    assert response.status_code == 200, response.json()
    assert page.get_text(user) == "Test Submission\n new line"
    assert OCRPageTranscription.objects.get(page=page).status == constants.TranscriptionStatus.pending

    # Test submitting invalid text
    response = client.post(f"/b/ocr/api/pages/{page.pk}/submit_transcription/", {"text": "Line1\nLine2\nLine3"})
    assert response.status_code == 400, response.json()
    assert response.json() == {"detail": "Line numbers don't match original"}

    # Test submitting no text at all
    response = client.post(f"/b/ocr/api/pages/{page.pk}/submit_transcription/")
    assert response.status_code == 400, response.json()
    assert response.json() == {"detail": "Missing text argument"}

    # Test submitting a transcription with invalid status
    response = client.post(
        f"/b/ocr/api/pages/{page.pk}/submit_transcription/", {"text": "Test Submission", "status": "auto_approved"}
    )
    assert response.status_code == 400, response.json()
    assert OCRPageTranscription.objects.get(page=page).status == constants.TranscriptionStatus.pending

    # Test submitting a transcription with a valid status
    response = client.post(
        f"/b/ocr/api/pages/{page.pk}/submit_transcription/", {"text": "Test Submission\nline2", "status": "submitted"}
    )
    assert response.status_code == 200, response.json()
    assert OCRPageTranscription.objects.get(page=page).status == constants.TranscriptionStatus.submitted


@pytest.mark.django_db
@freeze_time("2012-01-14T00:00:00Z")
def test_getting_page_transcriptions():
    page = factories.OCRPageFactory()
    client = APIClient()
    factories.OCRPageTranscriptionFactory(
        page=page,
        text="transcription1 \n ",
        status=constants.TranscriptionStatus.submitted,
    )
    factories.OCRPageTranscriptionFactory(
        page=page,
        text="transcription2 \n ",
        status=constants.TranscriptionStatus.manually_approved,
    )
    # ignored because it's not submitted
    factories.OCRPageTranscriptionFactory(
        page=page,
        text="transcription3 \n ",
        status=constants.TranscriptionStatus.pending,
    )
    # ignored because its a different page
    factories.OCRPageTranscriptionFactory(
        text="transcription3 \n ",
        status=constants.TranscriptionStatus.submitted,
    )
    response = client.get(f"/b/ocr/api/pages/{page.pk}/transcriptions/")
    assert response.status_code == 200, response.json()
    assert response.json() == {
        "rows": [
            {"username": "user_1", "text": "transcription1 \n ", "modified": "2012-01-14T00:00:00Z"},
            {"username": "user_2", "text": "transcription2 \n ", "modified": "2012-01-14T00:00:00Z"},
        ],
        "next_page_id": None,
    }


@pytest.mark.django_db
def test_submitting_transcription_review():
    page = factories.OCRPageFactory(
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"
    )
    complete_page = factories.OCRPageFactory(
        text="transcription_line1\ntranscription_line2",
        complete=True,
    )
    # two different users will submit transcriptions
    # admin will review their transcription
    # after review, each transcription should have approved_lines_count and rejected_lines_count

    user1 = factories.UserFactory(username="user1")
    user2 = factories.UserFactory(username="user2")
    admin_user = accounts_factories.AdminUserFactory(username="admin")
    client1 = APIClient()
    client2 = APIClient()
    client_admin = APIClient()

    client1.force_authenticate(user1)
    client2.force_authenticate(user2)
    client_admin.force_authenticate(admin_user)

    factories.OCRPageTranscriptionFactory(
        author=user1,
        page=page,
        text="transcription_line1\ntranscription_line2\ntranscription_line3 with extra text\ntranscription_line4",
        status=constants.TranscriptionStatus.submitted,
    )
    factories.OCRPageTranscriptionFactory(
        author=user2,
        page=page,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4",
        status=constants.TranscriptionStatus.submitted,
    )

    # if review got submitted without a 'text' argument, it would raise an error
    response = client_admin.post(
        f"/b/ocr/api/pages/{page.pk}/submit_transcription_review/",
    )
    assert response.status_code == 400
    assert response.json() == {"detail": "Missing text argument"}

    # if a review for a complete page is submitted, it would raise an error
    response = client_admin.post(
        f"/b/ocr/api/pages/{complete_page.pk}/submit_transcription_review/",
        {"text": "transcription_line1\ntranscription_line2"},
    )
    assert response.status_code == 400
    assert response.json() == {"detail": "This page is already complete!"}

    # non-admin users can not submit this api:
    response = client1.post(
        f"/b/ocr/api/pages/{page.pk}/submit_transcription_review/",
        {"text": "transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"},
    )
    assert response.status_code == 403

    # admin can submit
    response = client_admin.post(
        f"/b/ocr/api/pages/{page.pk}/submit_transcription_review/",
        {"text": "transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4"},
    )
    assert response.status_code == 200, response.json()

    # user1 should have 3 approved_lines_count and 1 rejected_lines_count,
    # while user2 should have 4 approved_lines_count and 0 rejected_lines_count

    user1_transcription = page.transcriptions.filter(author=user1).first()
    user2_transcription = page.transcriptions.filter(author=user2).first()

    assert user1_transcription.approved_lines_count == 3
    assert user1_transcription.rejected_lines_count == 1
    assert user2_transcription.approved_lines_count == 4
    assert user2_transcription.rejected_lines_count == 0

    # status of the transcriptions should be submitted (1)
    assert user1_transcription.status == constants.TranscriptionStatus.submitted
    assert user2_transcription.status == constants.TranscriptionStatus.submitted

    # admins review should be a new transcription, with the status of manually_approved
    admin_transcription = OCRPageTranscription.objects.get(page=page, author=admin_user)
    assert admin_transcription.status == constants.TranscriptionStatus.manually_approved


@pytest.mark.django_db
def test_get_translations():
    api_client = APIClient()
    user = accounts_factories.UserFactory(username="user", password="password")
    document = factories.OCRDocumentFactory()
    page1 = factories.OCRPageFactory(
        document=document,
        number=1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
    )

    page2 = factories.OCRPageFactory(
        document=document,
        number=2,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
    )
    page3 = factories.OCRPageFactory(
        document=document,
        number=3,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
    )

    # submit transcriptions for three pages
    factories.OCRPageTranscriptionFactory(
        page=page1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
        status=constants.TranscriptionStatus.submitted,
    )
    factories.OCRPageTranscriptionFactory(
        page=page2,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
        status=constants.TranscriptionStatus.submitted,
    )
    very_long_text = "a" * 160
    factories.OCRPageTranscriptionFactory(
        page=page3,
        text=very_long_text,
        status=constants.TranscriptionStatus.submitted,
    )
    # Translations
    factories.OCRTranslationFactory(  # Submitted (only author could see it)
        page=page1, author=user, text="transcription_line1", translation="first line translation", number=1
    )
    factories.OCRTranslationFactory(  # Approved, even annonymous user could see it
        page=page1,
        author=user,
        text="transcription_line2",
        translation="second line translation",
        comment="test comment",
        status=constants.TranslationStatus.approved,
        number=2,
    )
    # without user being logged in, it should not allow the user to submit translation
    response = api_client.post(f"/b/ocr/api/pages/{page1.pk}/translations/", {})
    assert response.status_code == 403, response.json()
    # without user being logged in, they should still be able to view approved translation
    response = api_client.get(f"/b/ocr/api/pages/{page1.pk}/translations/")
    assert response.status_code == 200, response.json()
    assert response.json() == {
        "lines": [
            {"comment": "test comment", "text": "transcription_line2", "translation": "second line translation"},
        ],
        "next_page_text": "transcription_line1\n"
        "transcription_line2\n"
        "transcription_line3\n"
        "transcription_line4\n"
        "transcription_line5",
    }

    api_client.force_authenticate(user)

    # now it should allow the user to get the api
    response = api_client.get(f"/b/ocr/api/pages/{page1.pk}/translations/")
    assert response.status_code == 200, response.json()

    # the response should also contain the first 5 lines of the next line:
    assert response.json() == {
        "lines": [
            {
                "text": "transcription_line1",
                "translation": "first line translation",
                "comment": "",
            },
            {
                "text": "transcription_line2",
                "translation": "second line translation",
                "comment": "test comment",
            },
        ],
        "next_page_text": "\n".join(
            [
                "transcription_line1",
                "transcription_line2",
                "transcription_line3",
                "transcription_line4",
                "transcription_line5",
            ]
        ),
    }
    # test if the next page has more than 150 characters
    response = api_client.get(f"/b/ocr/api/pages/{page2.pk}/translations/")
    assert response.status_code == 200, response.json()
    assert response.json() == {
        "lines": [
            {
                "text": "transcription_line1",
                "translation": None,
                "comment": None,
            },
            {
                "text": "transcription_line2",
                "translation": None,
                "comment": None,
            },
            {
                "text": "transcription_line3",
                "translation": None,
                "comment": None,
            },
            {
                "text": "transcription_line4",
                "translation": None,
                "comment": None,
            },
            {
                "text": "transcription_line5",
                "translation": None,
                "comment": None,
            },
        ],
        "next_page_text": "".join(["a" * 150, " ..."]),
    }

    # check if there is no next page
    response = api_client.get(f"/b/ocr/api/pages/{page3.pk}/translations/")
    assert response.status_code == 200, response.json()
    assert response.json() == {
        "lines": [
            {
                "text": f"{very_long_text}",
                "translation": None,
                "comment": None,
            }
        ],
        "next_page_text": "",
    }


@pytest.mark.django_db
def test_submit_translations():
    api_client = APIClient()
    document = factories.OCRDocumentFactory()
    user = accounts_factories.UserFactory(username="user", password="password")
    api_client.force_authenticate(user)
    page1 = factories.OCRPageFactory(
        document=document,
        number=1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
    )

    # submit transcriptions for the page
    factories.OCRPageTranscriptionFactory(
        page=page1,
        text="transcription_line1\ntranscription_line2\ntranscription_line3\ntranscription_line4\ntranscription_line5",
        status=constants.TranscriptionStatus.submitted,
    )

    # Translation body
    translation_body = [
        {
            "text": "transcription_line1",
            "translation": "first line translation",
            "comment": "first line comment",
        },
        {
            "text": "transcription_line2",
            "translation": "second line translation",
            "comment": "second line comment",
        },
        {
            "text": "transcription_line3",
            "translation": "third line translation",
            "comment": "third line comment",
        },
        {
            "text": "transcription_line4",
            "translation": "fourth line translation",
            "comment": "fourth line comment",
        },
        {
            "text": "transcription_line5",
            "translation": "fifth line translation",
            "comment": "fifth line comment",
        },
    ]

    response = api_client.post(
        f"/b/ocr/api/pages/{page1.pk}/translations/",
        data=translation_body,
        format="json",
    )
    assert response.status_code == 201, response.json()
