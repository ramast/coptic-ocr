# Generated by Django 4.0.6 on 2023-10-24 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ocr", "0013_rename_transcription_ocrpagetranscription_text"),
    ]

    operations = [
        migrations.AddField(
            model_name="ocrpagetranscription",
            name="approved_lines_count",
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name="ocrpagetranscription",
            name="rejected_lines_count",
            field=models.IntegerField(default=0),
        ),
    ]
