# Generated by Django 4.0.6 on 2022-09-04 03:06

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("ocr", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="ocrline",
            options={"ordering": ("page", "number")},
        ),
        migrations.AlterModelOptions(
            name="ocrlinetranscription",
            options={"ordering": ("page", "line")},
        ),
        migrations.AlterModelOptions(
            name="ocrpage",
            options={"ordering": ("-document_id", "number")},
        ),
        migrations.AlterUniqueTogether(
            name="ocrline",
            unique_together={("page", "number")},
        ),
        migrations.AlterUniqueTogether(
            name="ocrlinetranscription",
            unique_together={("author", "line")},
        ),
        migrations.AlterUniqueTogether(
            name="ocrpage",
            unique_together={("document", "number")},
        ),
    ]
