import rules

from accounts.rules import is_admin

# OCR Document
rules.add_perm("ocr.view_ocrdocument", rules.predicates.always_allow)
rules.add_perm("ocr.change_ocrdocument", is_admin)
rules.add_perm("ocr.add_ocrdocument", is_admin)
rules.add_perm("ocr.delete_ocrdocument", is_admin)
rules.add_perm("ocr.submit_transcription_review", rules.is_staff)
# OCR Page
rules.add_perm("ocr.view_ocrpage", rules.predicates.always_allow)
rules.add_perm("ocr.change_ocrpage", is_admin)
rules.add_perm("ocr.add_ocrpage", is_admin)
rules.add_perm("ocr.delete_ocrpage", is_admin)
rules.add_perm("ocr.submit_transcription_ocrpage", rules.is_active)
