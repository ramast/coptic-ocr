from django.db.models import IntegerChoices


class TranscriptionStatus(IntegerChoices):
    pending = 0  # User saved the transcription
    submitted = 1  # User believe their work is done and has submitted their transcription
    auto_approved = 2
    manually_approved = 3
    ocr_training_ready = 4
    ocr_training_done = 5
    skipped = 10  # Lines should normally be skipped by adding @ before them
    rejected = 11


class TranslationStatus(IntegerChoices):
    submitted = 1  # User believe their work is done and has submitted their transcription
    approved = 2
