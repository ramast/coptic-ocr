from django.db.models import Count
from rest_framework import decorators, exceptions, viewsets
from rest_framework.response import Response
from rules.contrib.rest_framework import AutoPermissionViewSetMixin

from . import constants, models, serializers


class OCRDocumentViewset(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = models.OCRDocument.objects.all()
    pagination_class = None

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": None})
        return context

    def get_serializer_class(self):
        if self.kwargs.get("pk"):
            return serializers.OCRDocumentDetailSerializer
        return serializers.OCRDocumentSerializer


class OCRPageViewset(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = models.OCRPage.objects.all()
    pagination_class = None
    serializer_class = serializers.OCRPageDetailSerializer

    permission_type_map = {
        **AutoPermissionViewSetMixin.permission_type_map,
        "submit_transcription": "submit_transcription",
        "submit_transcription_review": "submit_transcription_review",
        "transcriptions": "view",
        "translations": "view",
    }

    def list(self, request, *args, **kwargs):
        raise exceptions.PermissionDenied()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"user": self.request.user, "request": None})
        return context

    @decorators.action(["post"], detail=True)
    def submit_transcription(self, request, **kwargs):
        page: models.OCRPage = self.get_object()
        try:
            submitted_text = request.data["text"]
        except KeyError:
            raise exceptions.ValidationError({"detail": "Missing text argument"})
        transcription_status = request.data.get("status", "pending")
        if transcription_status not in {"pending", "submitted"}:
            raise exceptions.ValidationError({"detail": "Incorrect status"})
        # convert transcription status from string to integer value
        transcription_status = constants.TranscriptionStatus[transcription_status]
        page.set_text(submitted_text, request.user, transcription_status)
        return Response(status=200)

    @decorators.action(["post"], detail=True)
    def submit_transcription_review(self, request, **kwargs):
        page: models.OCRPage = self.get_object()
        try:
            approved_text = request.data["text"]
        except KeyError:
            raise exceptions.ValidationError({"detail": "Missing text argument"})
        if page.complete:
            raise exceptions.ValidationError({"detail": "This page is already complete!"})
        page.set_text(approved_text, request.user, constants.TranscriptionStatus.manually_approved)

        # loop through all the submitted transcriptions for this page:

        # compare the manually_approved text with all the transcriptions line by line
        # and count approved and rejected lines for each transcription
        for user_transcription in page.transcriptions.filter(status=constants.TranscriptionStatus.submitted):
            user_transcription_text = user_transcription.text.split("\n")
            approved_lines_count = 0
            rejected_lines_count = 0
            for index, line in enumerate(approved_text.split("\n")):
                if line == user_transcription_text[index]:
                    approved_lines_count += 1
                else:
                    rejected_lines_count += 1

            # insert the counts on OCRPageTranscription objects
            user_transcription.approved_lines_count = approved_lines_count
            user_transcription.rejected_lines_count = rejected_lines_count
            user_transcription.save()

        return Response()

    @decorators.action(["get"], detail=True)
    def transcriptions(self, request, **kwargs):
        page: models.OCRPage = self.get_object()
        transcription_serializer = serializers.OCRPageTranscriptionsSerializer(
            page.transcriptions.published,
            many=True,
        )
        transcription_data = {"rows": transcription_serializer.data}
        next_pages = (
            models.OCRPage.objects.filter(id__gt=page.id, complete=False)
            .annotate(transcriptions_count=Count("transcriptions"))
            .filter(transcriptions_count__gt=1)
            .values_list("pk", flat=True)
        )
        try:
            transcription_data["next_page_id"] = next_pages[0]
        except IndexError:
            transcription_data["next_page_id"] = None

        return Response(transcription_data)

    @decorators.action(["get", "post"], detail=True)
    def translations(self, request, **kwargs):
        page: models.OCRPage = self.get_object()
        page_id = kwargs.get("pk")
        author_id = request.user.pk

        # Check the HTTP method
        if request.method == "GET":
            # Handle GET request using OCRPageTranslationSerializer
            serializer = serializers.OCRPageTranslationSerializer(
                instance=page,
                context={"user": request.user},
            )
            return Response(serializer.data, status=200)
        # Manually check for user's permission
        if not request.user.is_active:
            raise exceptions.PermissionDenied()
        # Handle POST request using OCRLineTranslationSerializer
        serializer = serializers.OCRLineTranslationSerializer(
            data=request.data,
            context={"author_id": author_id, "page_id": page_id},
            many=True,
        )
        serializer.is_valid(raise_exception=True)
        # If there are existing translations for the page from this author, we should delete them first
        page.translations.filter(author_id=author_id).delete()
        # Remove any approved translation status
        page.transcriptions.update(status=constants.TranslationStatus.submitted)
        # Pass the line number field before saving
        for line_index, line in enumerate(serializer.validated_data):
            line["number"] = line_index + 1
            # For now we automatically make latest translation approved
            line["status"] = constants.TranslationStatus.approved
        # Pass the page_id obtained from the URL to the save method
        serializer.save()
        return Response(status=201)
