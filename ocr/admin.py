from django.contrib import admin

from . import models


@admin.register(models.OCRDocument)
class OCRDocumentAdmin(admin.ModelAdmin):
    """
    Admin page for all documents
    """


@admin.register(models.OCRPage)
class OCRPageAdmin(admin.ModelAdmin):
    """
    Admin page for all Pages
    """


@admin.register(models.OCRPageTranscription)
class OCRPageTranscriptionAdmin(admin.ModelAdmin):
    """
    Admin page for all Pages
    """


@admin.register(models.OCRLineTranslation)
class OCRLineTranslationAdmin(admin.ModelAdmin):
    """
    Admin page for all Pages
    """
