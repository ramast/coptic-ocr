from django.db import models

from . import constants


class OCRPageTranscriptionManager(models.Manager):
    @property
    def published(self):
        return self.filter(
            status__in=[
                constants.TranscriptionStatus.submitted,
                constants.TranscriptionStatus.manually_approved,
                constants.TranscriptionStatus.auto_approved,
            ]
        )
