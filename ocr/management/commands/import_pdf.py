import glob
import os
import re
import sys

from django.conf import settings
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand

# using html is unsafe for untrusted xml documents but in our case it comes from tesseract which we trust
from lxml import html  # noqa: S410

from ocr import models

# parses XML's box attribute
BOX_RE = re.compile(r"bbox (\d+) (\d+) (\d+) (\d+);.*")
LINE_NUMBER_RE = re.compile(r"line_1_(\d+)")


def raise_error(message: str, exist_status: int):
    print(message, file=sys.stderr)
    sys.exit(exist_status)


class Command(BaseCommand):
    help = "Import a PDF file for OCR processing"

    def add_arguments(self, parser):
        parser.add_argument("filename", type=str)

    def parse_box_data(self, box_str: str) -> tuple:
        return [int(number) for number in BOX_RE.match(box_str).groups()]

    def import_hocr_file(self, hocr_filename, ocr_page: "models.OCRPage"):
        # Document root
        root = html.parse(hocr_filename).getroot()
        lines = []
        for line in root.find_class("ocr_line"):
            words = [
                {"text": word.text.lower(), "box": self.parse_box_data(word.attrib["title"])}
                for word in line.find_class("ocrx_word")
            ]

            line_data = {
                "box": self.parse_box_data(line.attrib["title"]),
                "words": words,
            }
            lines.append(
                models.OCRLine(
                    page_id=ocr_page.pk,
                    number=int(LINE_NUMBER_RE.match(line.attrib["id"])[1]),
                    ocr_data=line_data,
                )
            )
        models.OCRLine.objects.bulk_create(lines)

    def add_document(self, filename: str) -> "models.OCRDocument":
        if not filename.lower().endswith(".pdf"):
            raise_error("only PDF files are supported", 1)
        if not os.path.isfile(filename):
            raise_error(f"No such file or directory: {filename}", 1)
        # Copy the file to default storage
        new_filename = "books/{0}".format(os.path.basename(filename).lower().replace(" ", "_"))
        if models.OCRDocument.objects.filter(filename=new_filename):
            raise_error("This book seems to have been already imported", 2)
        with open(filename, "rb") as original_file:
            new_filename = default_storage.save(new_filename, original_file)

        return models.OCRDocument.objects.create(
            filename=new_filename, title=os.path.basename(filename).replace("-", " ")[:-4]
        )

    def handle(self, *args, **kwargs):  # noqa: WPS110
        os.chdir(settings.MEDIA_ROOT)
        filename = kwargs["filename"]
        document = self.add_document(filename)
        dirname = document.filename.path[:-4]
        try:
            os.mkdir(dirname)
        except FileExistsError:
            raise_error(
                (
                    "This file seems to have been imported before\n"
                    "If this is not the case, please be sure to rename it first"
                ),
                2,
            )
        os.chdir(dirname)
        if os.system(f"pdftoppm -hide-annotations -gray -png -r 200 '{filename}' image"):  # noqa: S605
            sys.exit(3)
        images = sorted(glob.glob("image-*.png"))
        for index, image in enumerate(images):
            hocr_file = image.rsplit(".", maxsplit=1)[0]
            failed = os.system(  # noqa: S605
                (
                    f"tesseract '{image}' '{hocr_file}' --tessdata-dir '{settings.TESSDATA_PREFIX}' --oem 1 --psm 6"
                    " -l cop /usr/share/tessdata/configs/hocr"
                )
            )
            if failed:
                sys.exit(4)  # exit in case of error
            # Create a page object
            dirname = dirname.replace(default_storage.location, "")
            page: "models.OCRPage" = models.OCRPage.objects.create(
                document_id=document.pk, image=f"{dirname}/{image}", hocr_file=hocr_file, number=index + 1
            )
            self.import_hocr_file(f"{hocr_file}.hocr", page)
