import hashlib
import typing

from django.core import exceptions
from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property
from model_helpers import upload_to
from rules.contrib import models as rules_models

from . import constants, managers

if typing.TYPE_CHECKING:
    from accounts.models import User


class OCRDocument(rules_models.RulesModel):
    filename = models.FileField(blank=False, null=False, help_text="PDF file", upload_to="books/pending")
    title = models.CharField(max_length=120, blank=True, default="", help_text="Book / Document title", unique=True)
    description = models.TextField(blank=True, default="", help_text="Book / Document description")
    image = models.ImageField(blank=True, null=True, default=None, upload_to=upload_to)

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Book"

    def __str__(self):
        return self.title


class OCRPage(rules_models.RulesModel):
    document = models.ForeignKey(OCRDocument, related_name="pages", on_delete=models.CASCADE, null=False)
    image = models.ImageField(null=False, help_text="image of that page")
    hocr_file = models.FileField(null=False, help_text="Tesseract generated hocr file")
    number = models.PositiveIntegerField(help_text="page number")
    complete = models.BooleanField(default=False, blank=True)
    text = models.TextField(default=None, blank=True, null=True)
    lines_data = models.JSONField(
        null=True,
        blank=True,
        default=None,
        help_text="Hold information about line position in the page",
    )

    class Meta:
        ordering = ("-document_id", "number")
        unique_together = ("document", "number")

    def __str__(self):
        title = self.document.title
        return f"{title} ({self.number})"

    def set_text(
        self,
        user_text: str,
        user: "User",
        transcription_status=constants.TranscriptionStatus.pending,
    ):
        user_text = user_text.replace("\r", "")
        original_text = self.text
        if user_text.count("\n") != original_text.count("\n"):
            raise exceptions.ValidationError("Line numbers don't match original")

        OCRPageTranscription.objects.update_or_create(
            author=user,
            page=self,
            defaults={
                "text": user_text,
                "status": transcription_status,
                "modified": timezone.now(),
            },
        )

        # if the transcription is manually_approved, then the page should be marked as complete
        if transcription_status == constants.TranscriptionStatus.manually_approved:
            self.complete = True
            self.save()
            return  # TODO check it later if this is fine

        # if the transcription is just submitted by user, see if the page could be auto approved
        self.auto_approve()

    def get_text(self, user: "User" = None) -> str:
        if not user:
            return ""
        try:
            return self.transcriptions.filter(author_id=user.pk).values_list("text", flat=True)[0]
        except IndexError:
            return ""

    @property
    def best_transcription(self) -> str:
        transcription = (
            self.transcriptions.exclude(
                status__in=(
                    constants.TranscriptionStatus.pending,
                    constants.TranscriptionStatus.rejected,
                )
            )
            .order_by("-status")
            .only("text")
            .first()
        )
        if transcription:
            return transcription.text
        return ""

    def translation(self, author_id: int) -> list["OCRLineTranslation"]:
        if author_id:
            translations = self.translations.filter(author_id=author_id)
        else:
            translations = self.translations.filter(status=constants.TranslationStatus.approved)
        if translations:
            return list(translations)
        best_transcription_lines = self.best_transcription.split("\n")
        return [
            OCRLineTranslation(text=line, translation=None, comment=None, number=line_idx + 1)
            for line_idx, line in enumerate(best_transcription_lines)
        ]

    @cached_property
    def next_page(self) -> typing.Optional["OCRPage"]:
        return OCRPage.objects.filter(document=self.document, number__gt=self.number).order_by("number").first()

    def auto_approve(self):
        """
        Find transcriptions of same page that has exact same text and auto approve it.
        """
        if self.complete:
            # If page has already been marked as complete, there is no need for further checks.
            return
        agreeing_transcriptions = (
            self.transcriptions.filter(status=constants.TranscriptionStatus.submitted)
            .values("page_id", "checksum")
            .annotate(count=models.Count("*"))
            .filter(count__gt=1)
            .values("page_id", "checksum")
        )
        if not agreeing_transcriptions:
            # No agreeing transcription, nothing that needs to be done further
            return
        for transcription in agreeing_transcriptions:
            # Start by rejecting all transcriptions for that line
            self.transcriptions.filter(page_id=transcription["page_id"]).update(
                status=constants.TranscriptionStatus.rejected
            )
            # Approve the ones that do agree
            self.transcriptions.filter(**transcription).update(status=constants.TranscriptionStatus.auto_approved)

        self.complete = True
        self.save()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.text = self.text.replace("\r", "")
        super().save(force_insert, force_update, using, update_fields)


class OCRPageTranscription(rules_models.RulesModel):
    author = models.ForeignKey(
        "accounts.User", null=False, on_delete=models.CASCADE, related_name="user_transcriptions"
    )
    page = models.ForeignKey(OCRPage, on_delete=models.CASCADE, null=False, related_name="transcriptions")
    text = models.TextField(blank=True, null=True, help_text="Corrected OCR text")
    checksum = models.CharField(max_length=40, blank=False, null=False)
    status = models.PositiveSmallIntegerField(
        choices=constants.TranscriptionStatus.choices, default=constants.TranscriptionStatus.pending
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    approved_lines_count = models.IntegerField(default=0)
    rejected_lines_count = models.IntegerField(default=0)
    objects = managers.OCRPageTranscriptionManager()  # noqa: WPS110

    class Meta:
        ordering = ("page",)
        unique_together = ("author", "page")

    def get_checksum(self):
        return hashlib.sha1(self.text.encode("utf8"), usedforsecurity=False).hexdigest()

    def save(self, *args, **kwargs):
        self.text = self.text.replace("\r", "")
        self.checksum = self.get_checksum()
        return super().save(*args, **kwargs)

    def __str__(self):
        transcription_preview = self.text[:20]
        return f"{transcription_preview} - {self.page}"


class OCRLineTranslation(rules_models.RulesModel):
    author = models.ForeignKey(
        "accounts.User",
        null=False,
        on_delete=models.CASCADE,
        related_name="user_translations",
    )
    page = models.ForeignKey(
        OCRPage,
        on_delete=models.CASCADE,
        null=False,
        related_name="translations",
    )
    number = models.IntegerField(help_text="Line number")
    text = models.CharField(max_length=255, help_text="Original text")
    translation = models.CharField(max_length=255, blank=True, null=True, help_text="User's translation")
    comment = models.TextField(blank=True, null=True, help_text="User's comment")
    status = models.PositiveSmallIntegerField(
        choices=constants.TranslationStatus.choices, default=constants.TranscriptionStatus.submitted
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("page", "number")
        unique_together = ("author", "page", "number")

    def __str__(self):
        return f"[{self.number}] {self.text}"
