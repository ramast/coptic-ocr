from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import api

router = DefaultRouter()
router.register("documents", api.OCRDocumentViewset)
router.register("pages", api.OCRPageViewset)

urlpatterns = [
    path("api/", include(router.urls)),
]
