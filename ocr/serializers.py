from rest_framework import serializers

from . import models


class OCRPageSerializer(serializers.ModelSerializer):
    url = serializers.FileField(use_url=True, source="image")
    document_id = serializers.SerializerMethodField()
    document_title = serializers.SerializerMethodField()

    class Meta:
        model = models.OCRPage
        fields = ("pk", "number", "url", "document_id", "document_title")

    @staticmethod
    def get_document_id(page: "models.OCRPage"):
        return page.document.id

    @staticmethod
    def get_document_title(page: "models.OCRPage"):
        return page.document.title


class OCRPageDetailSerializer(OCRPageSerializer):
    transcription = serializers.SerializerMethodField()
    submitted = serializers.SerializerMethodField()
    next_page_complete = serializers.SerializerMethodField()
    total_number_of_transcriptions = serializers.SerializerMethodField()
    ocr_text = serializers.CharField(source="text")

    def get_transcription(self, page: "models.OCRPage"):
        if page.complete:
            # If page is complete, return the final accepted transcription
            return page.best_transcription
        user = self.context["user"]
        return page.get_text(user)

    def get_submitted(self, page: "models.OCRPage"):
        user = self.context["user"]
        if not user.is_active:
            return False
        return models.OCRPageTranscription.objects.published.filter(
            page=page,
            author=user,
        ).exists()

    @staticmethod
    def get_total_number_of_transcriptions(page: "models.OCRPage"):
        return page.transcriptions.published.count()

    @staticmethod
    def get_next_page_complete(page: "models.OCRPage"):
        # If there is a next page, return its 'complete' status
        return (page.next_page is None) or page.next_page.complete

    class Meta:
        model = models.OCRPage
        fields = (
            "pk",
            "number",
            "url",
            "ocr_text",
            "transcription",
            "complete",
            "next_page_complete",
            "submitted",
            "lines_data",
            "total_number_of_transcriptions",
        )


class OCRDocumentSerializer(serializers.ModelSerializer):
    first_page_id = serializers.SerializerMethodField()

    @staticmethod
    def get_first_page_id(document: "models.OCRDocument"):
        return document.pages.values_list("id", flat=True).first()

    class Meta:
        model = models.OCRDocument
        fields = ("pk", "filename", "title", "image", "description", "first_page_id")


class OCRDocumentDetailSerializer(serializers.ModelSerializer):
    url = serializers.FileField(use_url=True, source="filename")
    pages = OCRPageSerializer(many=True)

    class Meta:
        model = models.OCRDocument
        fields = ("pk", "title", "url", "description", "pages")


class OCRPageTranscriptionsSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    @staticmethod
    def get_username(transcription: "models.OCRPageTranscription"):
        return transcription.author.username

    class Meta:
        model = models.OCRPageTranscription
        fields = ("username", "text", "modified")


class OCRLineTranslationSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        models.OCRLineTranslation.objects.create(
            author_id=self.context["author_id"],
            page_id=self.context["page_id"],
            number=validated_data.pop("number"),
            **validated_data,
        )

    class Meta:
        model = models.OCRLineTranslation
        fields = ("text", "translation", "comment")


class OCRPageTranslationSerializer(serializers.Serializer):
    lines = serializers.SerializerMethodField()
    next_page_text = serializers.SerializerMethodField()

    @staticmethod
    def get_next_page_text(page: "models.OCRPage"):
        if page.next_page:
            transcription = page.next_page.best_transcription
            if len(transcription) <= 150:
                return transcription
            return "{0} ...".format(transcription[:150])
        return ""

    def get_lines(self, page: "models.OCRPage"):
        author_id = self.context["user"].pk
        return OCRLineTranslationSerializer(page.translation(author_id), many=True).data
