import React from 'react'
import { render, screen } from '@testing-library/react'
import { DocumentGallery } from '../../components/documents/DocumentGallery'

test('renders current date', () => {
    render(<DocumentGallery />)
    const title = screen.getByText(/Hello World/i)
    expect(title).toBeInTheDocument()
})
