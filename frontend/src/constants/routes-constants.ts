export const ROUTES = {
    HOMEPAGE_ROUTE: '/',
    REGISTER_ROUTE: '/register',
    USER_PROFILE_ROUTE: '/profile',
    DOCUMENT_DETAILS_ROUTE: '/book/:id/:page',
    DOCUMENT_REVIEW_ROUTE: '/book/:id/:page/review',
    DOCUMENT_TRANSLATE_ROUTE: '/book/:id/:page/translate',
}
